
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,TouchableOpacity,StatusBar,Dimensions,Image,FlatList,AsyncStorage
} from 'react-native';
import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content,TabHeading, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator,
    Item, Input, Spinner,Card, CardItem,
} from 'native-base';
var SQLite = require('react-native-sqlite-storage');
let db = SQLite.openDatabase({name: 'test.db', createFromLocation : "~Harrison.db", location: 'Library'}, this.openCB, this.errorCB);
import urldetails from '../config/endpoints.json';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default class Cart extends Component {
state={
name:'',
price:'',
t_item:0,
total:0,
cart:[],
minorder: 0,
logedin: false,
spinner:true,
  };
  componentWillMount() {
   this.refreshcart()
   AsyncStorage.getItem("user")
      .then((value) => {
        const values = JSON.parse(value)
        console.log(values)
        if (values == null || values == undefined) {
          this.setState({ logedin: false })
          console.log("false")
        }
        else {
          console.log("true")
          this.setState({ logedin: true })
        }
      })
  }
  delete(id) {
    db.transaction((ca) => {
      ca.executeSql('select * from cart where id=?', [id], (ca, caresult) => {
        let row = caresult.rows.item(0);
          db.transaction((del) => {
            del.executeSql('DELETE FROM cart WHERE id=?', [id], (del, delresult) => {
              console.log("item deleted ")
              // this.setState({})
              this.refreshcart()
            })
          })
      })
    })
  }
  minus(id, qty) {
    if (qty > 1) {
      db.transaction((mi) => {
        mi.executeSql('update cart set qty=? where id=?', [parseInt(qty) - 1, id], (mi, miresult) => {
          console.log("item substracted ")
          this.refreshcart()
        })
      })
    }
  }
  plus(id, qty) {
    db.transaction((pl) => {
      pl.executeSql('update cart set qty=? where id=?', [parseInt(qty) + 1, id], (pl, plresult) =>{
        console.log("item added ")
        this.refreshcart()
        
      })
    })
  }
  refreshcart() {
    db.transaction((tx) => {
            tx.executeSql('SELECT * FROM cart', [], (tx, results) => {
                console.log("Query completed");
                lcart = [];
                item = 0;
                 t = 0;
                var len = results.rows.length; 
               for (let i = 0; i < len; i++) {
                    let row = results.rows.item(i);
                    lcart.push(row);
                    t = t + parseInt(row.qty * row.p_price);
                    item = i + 1;
                    console.log(row.p_name)
                    this.setState({ cart: lcart, t_item: item, total: t,spinner:false })
                    console.log(this.state.cart) 
               }   
            });
      })
  }
  clearall() {
    db.transaction((del) => {
      del.executeSql('DELETE FROM cart', [], (del, delresult) => {
        console.log("item deleted ")
        this.setState({ total: 0 })
        this.props.navigation.navigate("Home")
      })
    })
    }
    checkout() {
      if (this.state.total > 0) {
        if (this.state.total >= this.state.minorder) {
          if (this.state.logedin) {
            this.props.navigation.navigate('Shipping',
              {
                cart:this.state.cart,
                total: this.state.total,
              })
          }
          else {
            this.props.navigation.navigate('GuestLogin',
              {
                total: this.state.total,
              }
            )
          }
        
      }
        else {
          alert("Your order must be more than RS. " + this.state.minorder + " to proceed. Please add some more items. ")
        }
      }
      else {
        alert("Your Cart is Empty.")
      }
    }
  
  render() {   
    return (
        <Container>
              <Header style={{ backgroundColor: '#094872' }} hasTabs>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                            <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title>Cart</Title></Body>
                    {/* <Right>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Search")}>
                    <Icon name='search'  style={{ fontSize: 25, color: 'white',marginRight:20 }}/>
                    </TouchableOpacity>
                    <Icon name='ios-cart' style={{ fontSize: 25, color: 'white' }} />
                      </Right> */}
                </Header>
                <StatusBar backgroundColor="#1a1a1a" barStyle="light-content"  />
                 <Content>  
                 {this.state.spinner ? <View><Spinner color='#ffb400'/></View> : <View/>} 
                 <List style={{ backgroundColor: '#d9d9d9' }}>
                <ListItem>
                  <Body>
                    <View style={{ flexDirection: 'row' }}>
                      <Left>
                        <Text>ITEMS : {this.state.t_item} </Text>

                      </Left>

                      <Text note>SUB TOTAL Rs.{this.state.total }</Text>


                      <Right>
                        <TouchableOpacity onPress={() => this.clearall()}>
                          <Text>Clear all</Text>
                        </TouchableOpacity>
                      </Right>
                    </View>
                  </Body>

                </ListItem>
              </List>
                 <List dataArray={this.state.cart}
                renderRow={(cartdata) =>
                  <ListItem>
                    <Left>
                      <Text style={{ marginRight: 5 }}>{cartdata.p_name}</Text>
                    </Left>
                    <Left style={{ marginLeft: 60 }}>
                      {/* <TouchableOpacity onPress={() => this.minus(cartdata.id, cartdata.qty)} style={{ height: 18, width: 18, marginRight: 1, backgroundColor: "#ff0000", alignItems: "center", elevation: 4, }}>
                        <Text style={{ color: "white", fontSize: 13 }}>-</Text>
                      </TouchableOpacity> */}
                      <Text note>{cartdata.qty} Qty</Text>
                      {/* <TouchableOpacity onPress={() => this.plus(cartdata.id, cartdata.qty)} style={{ height: 18, width: 18, marginLeft: 1, backgroundColor: "#006400", alignItems: "center", elevation: 4, }}>
                        <Text style={{ color: "white", fontSize: 13 }}>+</Text>
                      </TouchableOpacity> */}
                    </Left>
                    <Left style={{ flexDirection: 'row', marginLeft: 40 }}>
                      <Text style={{ paddingRight: 5, paddingLeft: 5 }} note>Rs {cartdata.qty * cartdata.p_price}</Text>
                      <TouchableOpacity onPress={() => this.delete(cartdata.id)} style={{
                        height: 25, marginRight: 10,
                        width: 20, 
                        alignItems: "center"
                      }}>
                        <Icon name='md-trash' style={{ color: "#595959", fontSize: 20 }} />
                      </TouchableOpacity>
                    </Left>
                  </ListItem>
                }>
              </List>

               <List>
              <ListItem>
                 <Text style={{color:'black'}}>PRICE DETAILS</Text>
                  </ListItem>
                  <ListItem>
                      <Left>
                  <Text>Price({this.state.t_item}item)</Text>
                  </Left>
                  <Right>
                  <Text>Rs.{this.state.total}</Text>
                      </Right>
                      </ListItem>
                      <ListItem style={{alignContent:'center',alignItems:'center'}}>
                      <Text style={{marginLeft:'30%',fontSize:20}}>Total:</Text>
                      <Text style={{marginLeft:10}}>Rs.{this.state.total} </Text>
                      </ListItem>
                </List>
                </Content>
                <Footer style={{paddingTop:0}}>
            <FooterTab style={{ backgroundColor: '#fff' }}>
              <TouchableOpacity  style={{ backgroundColor: '#094872', width: screenwidth / 2 - 0.2 }}
              onPress={() => this.props.navigation.navigate('Home')}>
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                  {/* <FontAwesome style={{ color: '#fff', padding: 2,fontSize: 16 }} name="pencil-square-o" /> */}
                  <Text style={{ color: '#fff', padding: 2, fontSize: 16 }}>Add more</Text>
                  {/* <FontAwesome style={{ color: '#fff', paddingLeft: 2,fontSize: 16 }} name="pencil-square-o" /> */}
                </View>
              </TouchableOpacity>
              <TouchableOpacity  style={{ backgroundColor: '#094872', width: screenwidth / 2 - 0.2 }}
               onPress={() => this.checkout()}>
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                  <Text style={{ color: '#fff', padding: 2, fontSize: 16 }}>Continue</Text>
                </View>
              </TouchableOpacity>
            </FooterTab>
          </Footer>
            </Container>
    
    );
  }
}

