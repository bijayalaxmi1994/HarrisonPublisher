import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions, StatusBar,  AsyncStorage, Alert, ImageBackground,BackHandler } from 'react-native';
import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Item, Input, Spinner
} from 'native-base';
import Modal from "react-native-simple-modal";
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import urldetails from '../config/endpoints.json';
export default class Profile extends Component {
    constructor() {
        super();
        this.state = {
            open: false,
            name: '',
            email: '',
            contact: '',
            useridval: '',
            password:''
        }
    }
    componentWillMount() {
       
        AsyncStorage.getItem('user')
            .then((value) => {
                if (value != null || value != undefined) {
                    const values = JSON.parse(value)
                    console.log('user available' + values.userid)
                    this.setState({ useridval: values.userid, name:values.username,email:values.email,contact:values.contact,password:values.password })
                   console.log(values.email)
                }
            })
    }
    updateprofile() {
        this.setState({ spinner: true })
        if (this.state.name == "") {
            alert('Please Enter the name')
        }
        else {
            var formData = new FormData();
            formData.append('cid',this.state.useridval );
            formData.append('cname', this.state.name);

            fetch(urldetails.base_url + urldetails.url["updatecustomer"], {
                method: "POST",
                body: formData,
            })
                .then((response) => response.json())
                .then((jsondata) => {
                    console.log(jsondata)
                    if (jsondata.status === "success") {
                        alert(jsondata.message)
                        var userdetails={
                            useridval:jsondata.cid,
                            username:this.state.name,
                            contact:jsondata.ccontactno,
                            email:jsondata.cemail,
                            password:this.state.password,
                          }
                          AsyncStorage.setItem('user',JSON.stringify(userdetails));
                        //   console.log(userdetails)
                    }
                    else {
                        alert(jasondata.msg)
                    }
                    this.setState({ open: false, spinner: false })
                })
        }
    }
    modalDidOpen = () => console.log("Modal did open.");
    modalDidClose = () => {
      this.setState({ open: false });
      console.log("Modal did close.");
    };
    moveUp = () => this.setState({ offset: -100 });
    resetPosition = () => this.setState({ offset: 0 });
    openModal = () => this.setState({ open: true });
    closeModal = () => this.setState({ open: false });

    render() {
        return (

            <Container>
                <Content>
                <Header style={{ backgroundColor: '#094872' }} hasTabs>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                            <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title>Profile</Title></Body>
                    <Right>
                    <Icon name='arrow-back' style={{ fontSize: 25, color: 'white' }} />
                      </Right>
                </Header>
                <StatusBar backgroundColor="#000" barStyle="light-content" />
                        <View style={{ padding: 40, alignItems: 'center',backgroundColor:'#094872' }}>
                            <Thumbnail source={{uri:'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png'}} style={{ width: 55 }} />
                            <View style={{flexDirection:'row' }} >
                            <TouchableOpacity onPress={() => this.setState({open:true})}> 
                            <Text style={{color:'white',fontSize:24}}>{this.state.name}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({open:true})} > 
                            <FontAwesome style={{ color: '#fff', paddingLeft: 5,fontSize: 16 }} name="pencil-square-o" />
                            </TouchableOpacity>
                            </TouchableOpacity>
                            </View>
                            <Text style={{color:'white',fontSize:12}}>{this.state.email}</Text>
                        </View>
             
                    <View style={{ height: 40, marginTop: 25, marginBottom: 20, justifyContent: 'center', alignItems: 'center', }}>
            <TouchableOpacity style={{borderRadius:5,
                width: screenwidth - 40, height: 50, backgroundColor: "#ffb400",
                alignItems: "center", justifyContent: 'center', elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
              }}
              onPress={() => this.props.navigation.navigate('Changepassword')}>
                <Text style={{ color: "white", fontSize: 16 }}>CHANGE PASSWORD</Text>
              </TouchableOpacity>
            </View>
                </Content>
                <Modal style
offset={0}
open={this.state.open}
modalDidOpen={this.modalDidOpen}
modalDidClose={this.modalDidClose}
style={{ alignItems: "center", }}
>
<View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
<Left></Left>
<Text style={{ fontSize: 20, marginBottom: 10, textAlign: 'center' }}>Update your Profile</Text>
<Right>
<TouchableOpacity style={{ backgroundColor: 'red', borderRadius: 25, width: 25, height: 25 }} onPress={() => this.setState({ open: false })} >
<Text style={{ color: '#fff', fontWeight: 'bold', padding: 2, textAlign: 'center' }}>X</Text>
</TouchableOpacity>
</Right>
</View>
<View style={{ flexDirection:'column' ,alignContent: 'center', justifyContent: 'center',height:screenHeight/3 }}>
<Item regular style={{borderColor:'black'}}>
            <Input placeholder='Name' value={this.state.name} onChangeText={(name) => this.setState({ name: name })}/>
          </Item>
          <TouchableOpacity  style={{marginTop:15,height: 50,backgroundColor: "#ffb400",borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
  alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}} 
  onPress={() => this.updateprofile()}>
            <Text style={{ color: "white", fontSize: 20 }}>Update</Text>
          </TouchableOpacity>
          </View>
</Modal>
            </Container>
        );

    }
}
