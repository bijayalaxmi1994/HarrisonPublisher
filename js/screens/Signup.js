
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TouchableOpacity,Image,Dimensions,AsyncStorage,Alert
} from 'react-native';
import { Container, Header, Content,Left,Body,Title,Right,Item,Input,Thumbnail,Icon,Textarea} from 'native-base';
import urldetails from '../config/endpoints.json';
import { validateEmail, validatePhone, validateName, validatePassword } from '../config/validate';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-simple-modal';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
export default class Signup extends Component {
  state={
    name:'',
    email:'',
    contact:'',
    password:'',
    address:'',
    tokenval:'',
    useridval:'',
    spinner: false,
    open: false,  
  }
  registerUser(){
    if (this.state.name == "") {
      alert('Please enter your Name ..')
    }
    else if (!validateName(this.state.name)) {
      alert('Name must contain alphabet only..')
    }
    else if (this.state.email == "") {
      alert('Email field cannot be blank !')
    }
    // else if (!validateEmail(this.state.email)) {
    //   alert('Please enter valid email !')
    // }
    else if (this.state.contact == "") {
      alert('Phone number field cannot be blank !')
    }
    else if (!validatePhone(this.state.contact)) {
      alert('Please enter valid phone Number !')
    }
    else if (this.state.password == "") {
      alert('Password cannot be blank !')
    }
    else if (!validatePassword(this.state.password)) {
      alert('Password should contain one uppercase letter,one digit and should be of 6 characters ..')
    }
  
    else{
      
    this.setState({spinner:true})
    var formData = new FormData();
    formData.append('cname', this.state.name);
    formData.append('cemail', this.state.email);
    formData.append('ccontactno', this.state.contact);
    formData.append('cpassword', this.state.password);
    formData.append('caddress', this.state.address);
    console.log(formData);
    fetch(urldetails.base_url+urldetails.url["addcustomer"],{
      method: "POST",
       body: formData,
    })
    .then((response)=>response.json())
    .then((jsondata)=>{
      console.log(jsondata)
      if(jsondata.status=="success"){
        console.log(jsondata.message)
        Alert.alert('',jsondata.msg)
        // var userdetails={
          
        //   username:this.state.name,
        //   contact:this.state.contactno,
        //   email:this.state.email,
        //   password:this.state.password,
         
        // }
        // AsyncStorage.setItem('user',JSON.stringify(userdetails));
        // console.log(userdetails)
        // this.setState({ spinner: false,open:false })
        this.props.navigation.navigate("Home")
  }
  else{
    // Alert.alert('',jsondata.msg)
    this.setState({ spinner: false })
  }
}
)
.catch((error) => {
  this.setState({ spinner: false })
  console.log(error)
  
}); 
  }
}

  render() {
    return (
      <Container>
          <Content>
              <View style={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center',marginTop:80}}>
              <Text style={{textAlign:'center',color:'#094872',fontSize:26,fontWeight:'bold',marginTop:8,letterSpacing:3}}>SIGNUP</Text>
    <Item regular style={{width:screenwidth-40,alignItems:'center',backgroundColor:'#fff',marginTop:10,borderRadius:5,borderColor: '#c8c8c8'}}>
        <Input style={{fontSize:16,color:'#777'}} placeholder='Name' placeholderTextColor='#777' onChangeText={(name)=>this.setState({name: name})} value={this.state.name} autoCorrect={false} />
      </Item>
      <Item regular style={{width:screenwidth-40,alignItems:'center',backgroundColor:'#fff',marginTop:10,borderRadius:5,borderColor: '#c8c8c8'}}>
        <Input style={{fontSize:16,color:'#777'}} placeholder='Email' placeholderTextColor='#777'  onChangeText={(email)=>this.setState({email:email})} value={this.state.email} autoCapitalize="none" autoCorrect={false} />
      </Item>
      <Item regular style={{width:screenwidth-40,alignItems:'center',backgroundColor:'#fff',marginTop:10,borderRadius:5,borderColor: '#c8c8c8'}}>
        <Input style={{fontSize:16,color:'#777'}} placeholder='Contact no' placeholderTextColor='#777' onChangeText={(contact)=>this.setState({contact:contact})} autoCorrect={false}/>
      </Item>
      <Item regular style={{width:screenwidth-40,alignItems:'center',backgroundColor:'#fff',marginTop:10,borderRadius:5,borderColor: '#c8c8c8'}}>
        <Input style={{fontSize:16,color:'#777'}} placeholder='Password' placeholderTextColor='#777' onChangeText={(password)=>this.setState({password:password})} autoCorrect={false}/>
      </Item>
      <Textarea rowSpan={5} bordered placeholder="Address" placeholderTextColor='#777' onChangeText={(address)=>this.setState({address:address})} autoCorrect={false}
      style={{width:screenwidth-40,alignItems:'center',backgroundColor:'#fff',marginTop:10,borderRadius:5,borderColor: '#c8c8c8'}} />
      <View style={{height: 40,marginTop: 25,marginBottom: 20,justifyContent: 'center',alignItems: 'center',}}>
     <TouchableOpacity onPress={() => this.registerUser()} style={{width: screenwidth-40,height: 50,backgroundColor: "#094872",borderRadius:5,borderColor: '#c8c8c8',
alignItems: "center",justifyContent: 'center',elevation: 8,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}} 
>
          <Text style={{ color: "#fff", fontSize: 16 }} >SIGNUP</Text>
        </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => this.props.navigation.navigate("Login")} style={{marginTop:40}}>
        <Text style={{fontSize:15}}>Already have an account.Login here</Text>
        </TouchableOpacity>
         </View>
        
      </Content>
     </Container>
    );
  }
}


