import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions, StatusBar,StyleSheet, 
    AsyncStorage, Alert, ImageBackground,BackHandler,ScrollView,FlatList } from 'react-native';
import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail,
     List, ListItem, Tab, Tabs, ScrollableTab,
    Separator,Picker,Badge,
    Item, Input, Spinner,Card, CardItem
} from 'native-base';
import GridView from 'react-native-super-grid';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const Catagory=[{name:'Fiction'},{name:'Non-Fiction',name:'Novel'}];
import urldetails from '../config/endpoints.json';
var SQLite = require('react-native-sqlite-storage');
let db = SQLite.openDatabase({name: 'test.db', createFromLocation : "~Harrison.db", location: 'Library'}, this.openCB, this.errorCB);
export default class Books extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: '',
           genredata:[],
           tabdata: [],
            productsdata:[],
            currtab: 0,
            filtertype:'',
            p_id:'',
            id:this.props.navigation.state.params.id,  
            data:[],   
            t_item:'',
            spinner:true,
        };
      }
      componentWillMount(){
        var formData=new FormData();
        formData.append("pcat_id",this.state.id);
        fetch(urldetails.base_url+urldetails.url["products"],{
          method:'POST',
          body:formData
        }).then((response)=>response.json())
        .then((jsondata)=>{
            console.log(jsondata);
            if (jsondata.status == "success") {
                 console.log(jsondata.products);
                 this.setState({ productsdata: jsondata.products ,
                    genredata: jsondata.genre,spinner:false });
                    this.handleChangeTab(jsondata.genre[0].gen_name)
               }
              else {
                alert("Please try again..")
                this.setState({ spinner: false })
              }
        });
    this.fetchcartitem()   
}
fetchcartitem() {
  db.transaction((tx) => {
          tx.executeSql('SELECT * FROM cart', [], (tx, results) => {
              console.log("Query completed");
              lcart = [];
              item = 0;
               t = 0;
              var len = results.rows.length; 
             for (let i = 0; i < len; i++) {
                  let row = results.rows.item(i);
                  lcart.push(row);
                  t = t + parseInt(row.qty * row.p_price);
                  item = i + 1;
                  this.setState({ cart: lcart, t_item: item, total: t })
                  console.log(this.state.cart) 
             }   
          });
    })
}
    handleChangeTab(tabname) {
          this.filter_tab(tabname)
          //console.log("filter veg")  
      }
      filter_tab(tabname) {
        this.setState({ currtab: tabname,spinner:false })
        console.log("current tab " + tabname)
        var arr = []
        this.state.productsdata.map((myitem, i) => {       
          if (myitem.gen_name == tabname) {
            arr.push(myitem)
          }
          console.log(arr)
             this.setState({ tabdata: arr,spinner:false})
        })
      }
      onValueChange(value) {
        this.setState({
          selected: value
        });    
      }    
     render() {
      if (this.state.productsdata.length > 0 || this.state.genredata.length>0) {
        return (
        <Container>
                <Header style={{ backgroundColor: '#094872' }} hasTabs>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                          <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title></Title></Body>
                    <Right>
                        {/* <Icon name='search'  onPress={()=>this.props.navigation.navigate("Search")}
                         style={{ fontSize: 25, color: '#fff',marginTop:5,marginRight:10 }}/>   */}
                      <Badge style={{ backgroundColor: 'red',borderRadius: 25,width: 22, height: 22,marginBottom:8 }}>
            <Text style={{ color: 'white',marginLeft:2 }}>{this.state.t_item}</Text>
             </Badge>  
                    <Icon name='ios-cart' style={{ fontSize: 25, color: 'white' }} onPress={()=>this.props.navigation.navigate("Cart")} />
                  </Right>
                </Header>
                <StatusBar backgroundColor="#1a1a1a" barStyle="light-content" />
                <Content style={{backgroundColor:'#fff'}}>  
                <Tabs initialPage={0}  onChangeTab={(i, ref) => this.handleChangeTab(i.ref.props.heading)}
             tabBarUnderlineStyle={{ backgroundColor: 'white' }}
              renderTabBar={() => <ScrollableTab style={{ borderBottomWidth: 0, backgroundColor: '#094872', marginTop: 0.2 }} />}>
              {
                this.state.genredata.map((cat, i) => {
                  return (
                    <Tab heading={cat.gen_name} key={i}
                    tabStyle={{ backgroundColor: '#094872' }}
                    activeTabStyle={{ backgroundColor: '#094872' }}
                    textStyle={{ color: 'white',fontSize:20 }}
                    bottomTabBarBorderColor={{ color: '#094872' }}
                    activeTextStyle={{ color: 'white' }}>
                 <GridView
                itemDimension={130}
                items={this.state.tabdata}
                itemsPerRow={2}
                style={styles.gridView}
                renderItem={item =>(
                  <TouchableOpacity  style={{backgroundColor:'white',alignItems:"center",
                  justifyContent:"center",elevation:8,borderWidth:2,borderColor:'#e6e6e6',flexDirection:'column'}}
                            onPress={() =>this.props.navigation.navigate(("Bookdetails"),
                  {details:item  })}>
                              <Image source={{ uri: urldetails.image + item.image[0].sample_path, }}
                              style={{width:screenwidth/3.5,height:screenHeight/5,marginTop:7}} />
                              <Text style={{fontSize:15,padding:4,color:'#000'}}>{item.ptitle}</Text>
                              <Text style={{padding:4,marginBottom:10,color:'#0d3982'}}>Rs.{item.pprice}</Text>
                              </TouchableOpacity>
                
                      )}
                   />
                    </Tab>
                  )
                })
              }

            </Tabs>   
        
 
                 
        </Content> 
        </Container>
      
    );
  }
  else{
    return (
      <Container>
              <Header style={{ backgroundColor: '#094872' }} hasTabs>
                  <Left>
                      <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                        <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
                      </TouchableOpacity>
                  </Left>
                  <Body><Title></Title></Body>
                  <Right>
                      {/* <Icon name='search'  onPress={()=>this.props.navigation.navigate("Search")}
                       style={{ fontSize: 25, color: '#fff',marginTop:5,marginRight:10 }}/>   */}
                    <Badge style={{ backgroundColor: 'red',borderRadius: 25,width: 22, height: 22,marginBottom:8 }}>
          <Text style={{ color: 'white',marginLeft:2 }}>{this.state.t_item}</Text>
           </Badge>  
                  <Icon name='ios-cart' style={{ fontSize: 25, color: 'white' }} onPress={()=>this.props.navigation.navigate("Cart")} />
                </Right>
              </Header>
              <Content>
              {this.state.spinner ? <View style={{ marginTop: screenHeight / 3 }}><Spinner color='#ffb400' /><Text style={{ color: '#fff', textAlign: 'center' }}>Loading...</Text></View> : <View />}
                </Content>
              </Container>
    );
  }
}
}
const styles = StyleSheet.create({
    gridView: {
    //   padding:5,
      flex: 1,
      backgroundColor:'#fff',
    },
    itemContainer: {
      justifyContent: 'flex-end',
      borderRadius: 5,
      padding: 5,
    }
  })
  