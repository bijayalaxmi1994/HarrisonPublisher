/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,Dimensions,TouchableOpacity,StatusBar,Image,
} from 'react-native';
import {
  Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content,TabHeading, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
  Separator,
  Item, Input, Spinner,Card, CardItem,Radio
} from 'native-base';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default class Payments extends Component {
  render() {
    return (
      <Container>
         <Header style={{ backgroundColor: '#094872' }} hasTabs>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                            <Icon name='arrow-back' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title>Payments</Title></Body>
                </Header>
        <Content>
          <Card>
          <List>
            <ListItem selected={true} >
              <Left>
             <Text> Cash on Delivery</Text>
              </Left>
              <Right>
              <Radio
                color={"#f0ad4e"}
                selectedColor={"#5cb85c"}
                selected={true}
              />
                </Right>

              </ListItem>
              <ListItem selected={false}>
              <Left>
             <Text>Credit/Debit/ATM Card</Text>
              </Left>
              <Right>
              <Radio
                color={"#f0ad4e"}
                selectedColor={"#5cb85c"}
                selected={false}
              />
                </Right>
              </ListItem>
            </List>
            </Card>
            <Card>
            <List>
              <ListItem>
                 <Text style={{color:'black'}}>PRICE DETAILS</Text>
                  </ListItem>
                  <ListItem>
                      <Left>
                  <Text>Price(1 item)</Text>
                  </Left>
                  <Right>
                  <Text>Rs.690</Text>
                      </Right>
                      </ListItem>
                      <ListItem style={{alignContent:'center',alignItems:'center'}}>
                      <Left>
                      <Text style={{fontSize:20}}>Amount Payble</Text>
                      </Left>
                      <Right>
                      <Text >Rs.690 </Text>
                      </Right>

                          </ListItem>
              </List>
              </Card>
          </Content>
          <Footer style={{paddingTop:0}}>
            <FooterTab style={{ backgroundColor: '#fff' }}>
             <Right>
              <TouchableOpacity  style={{ backgroundColor: '#4598d3', width: screenwidth /2-0.2,height:screenHeight/15,marginRight:5 }}>
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                  <Text style={{ color: '#fff', padding: 2, fontSize: 16 }}>PLACE ORDER</Text>
                </View>
              </TouchableOpacity>
              </Right>
            </FooterTab>
          </Footer>
        </Container>
     
    );
  }
}
