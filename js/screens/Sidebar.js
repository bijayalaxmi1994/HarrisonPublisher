import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  StatusBar,
  Platform,
  TouchableWithoutFeedback,
  Dimensions,
  AsyncStorage,
} from 'react-native';
import {
  Container,
  Content,
  Body,
  Header,
  Label,
  ListItem,
  Thumbnail,Icon,
} from 'native-base';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
var navitems =[
  {name:'Home',nav:'Home',image:<Icon name='ios-home'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#7f8c8d'}}/>},
  {name:'Profile',nav:'Profile',image:<Icon name='ios-home'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#7f8c8d'}}/>},
  {name:'My Cart',nav:'Cart',image:<Icon name='ios-cart'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#7f8c8d'}}/>},
  {name:'Login',nav:'Login',image:<Icon name='ios-log-in'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#7f8c8d'}}/>},
  {name:'My Order',nav:'Order',image:<Icon name='ios-cart'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#7f8c8d'}}/>},
  {name:'Logout',nav:'Home',image:<Icon name='ios-log-out'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#7f8c8d'}}/>},
]
export default class Sidebar extends Component {
  render() {
    return (
        <Container>
      <View style={{borderWidth:0, flex:1, backgroundColor:'white'}}>
      <View style={{height:150,alignItems:"flex-start",
                      justifyContent: 'center',backgroundColor:'#2a3f54',paddingLeft:'35%'}}>
                      <Image source={{uri:'http://www.stickpng.com/assets/images/585e4bf3cb11b227491c339a.png'}} style={{width:80, height:80}} />
            <Text style={{color:'#fff', backgroundColor:'transparent', padding:4, textAlign:'center'}}>Welcome User </Text>
          </View>
        <Content style={{backgroundColor:'#2a3f54'}}>
        <View>
          {navitems.map((l,i,)=>{
            return (<ListItem key={i} style={{height:50}} onPress={()=>{
                  this.props.navigation.navigate(l.nav)}}>
            <View style={{flexDirection:'row',backgroundColor:'#fff0',marginRight:50,
                              }}>
                  {l.image}
            <Text style={{fontSize:18,marginLeft:16,marginTop:16,color:'white'}}>{l.name}</Text>
            </View></ListItem>)})
              }
        </View>
        </Content>
      </View>
      </Container>
    );
  }
}