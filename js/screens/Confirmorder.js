/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, Dimensions, AsyncStorage, TouchableOpacity, StatusBar, Alert, BackHandler } from 'react-native';
import{Container,Content,List,ListItem,Header,Body,Title,Item,Input,Left,Right,Spinner,Icon,Card,CardItem, Thumbnail,Footer, FooterTab} from 'native-base';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
import Modal from 'react-native-simple-modal';
var SQLite = require('react-native-sqlite-storage');
let db = SQLite.openDatabase({name: 'test.db', createFromLocation : "~biggies.db", location: 'Library'}, this.openCB, this.errorCB);

export default class Confirmorder extends Component {
    state={
      shipping_id:this.props.navigation.state.params.shipping_id,
        email:this.props.navigation.state.params.email,
        contactno:this.props.navigation.state.params.contactno,
        city:this.props.navigation.state.params.city,
        state:this.props.navigation.state.params.state,
        district:this.props.navigation.state.params.district,
        address:this.props.navigation.state.params.address,
        pincode:this.props.navigation.state.params.pincode,
        total:this.props.navigation.state.params.total,
        cart:[],
        p_id:"",
        p_name:"",
        p_price:"",
        qty:"",
        transaction_id:'',
      }
      componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
    
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
      }
      componentWillMount(){
        db.transaction((tx) => {
          tx.executeSql('SELECT * FROM cart', [], (tx, results) => {
              console.log("Query completed");
              lcart = [];
              item = 0;
               t = 0;
              var len = results.rows.length; 
             for (let i = 0; i < len; i++) {
                  let row = results.rows.item(i);
                  lcart.push(row);
                  t = t + parseInt(row.qty * row.p_price);
                  item = i + 1;
                  this.setState({ cart: lcart, t_item: item, total: t,p_id:row.p_id,qty:row.qty,p_name:row.p_name })
                   console.log(this.state.p_id) 
             }   
          });
    })
  }
  order(){
    var formData = new FormData();
    formData.append('tshipping_id',this.state.shipping_id);
    formData.append('tstatus',"pending");
    formData.append('tmode',"pending");
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["transaction"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
    .then((jsondata)=>{
      console.log(jsondata)
      if(jsondata.status=="success"){
        console.log(jsondata.transaction_data[0].transact_id)
        this.setState({ transaction_id: jsondata.transaction_data[0].transact_id});
        var formData = new FormData();
        formData.append('o_ship_id',this.state.shipping_id);
        formData.append('o_transaction_id',this.state. transaction_id);
        formData.append('total_price',this.state.total);
        console.log(formData)
        fetch(urldetails.base_url+urldetails.url["orders"],{
          method:"POST",
          body:formData
        })
        .then((response)=>response.json())
        .then((jsondata)=>{
          console.log(jsondata)
          if(jsondata.status=="success"){
            console.log(jsondata.order_data[0].oid)
            var orderdetails={
            oid:jsondata.order_data[0].oid,  
            }
            AsyncStorage.setItem('order',JSON.stringify(orderdetails));
            console.log(orderdetails)
            db.transaction((up)=>{
              up.executeSql('DELETE FROM cart',[],(up,upresult)=>{
                console.log("DELETE cart successfull")
              })
            })
            Alert.alert(
              'Order Placed',
              jsondata.message,
              [
                {text: 'OK', onPress: () => this.props.navigation.navigate('Home')},
              ],
              { cancelable: false }
            )
          }
        })


      }
      else{
        console.log(jsondata.message)
      }
     
      })    
      }
  render() {
    return (
        <Container>
        <Header style={{ backgroundColor: '#094872' }}>
          <Left>
            <TouchableOpacity style={{ padding: 5 }} onPress={() => this.props.navigation.navigate('Cart')}>
              <Icon name='arrow-back' style={{ fontSize: 25, color: 'white' }} />
            </TouchableOpacity>
          </Left>
          <Body style={{ alignContent: 'center' }}><Title>CONFIRM ORDER</Title></Body>
        </Header>
        <StatusBar backgroundColor="#000" barStyle="light-content" />
       <Content>
       {this.state.spinner ? <View><Spinner color='#ffb400' /></View> : <View />}
              <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', padding: 10 }}>Shipping Details</Text>
              <List style={{ marginTop: 2 }}>
                <ListItem>
                 <Body>
                    <Text style={{ fontSize: 16, color: '#262626' }}>{this.state.name}</Text>
                    <Text style={{ fontSize: 16, color: '#262626' }}>{this.state.email} </Text>
                   <Text style={{ fontSize: 16, color: '#262626' }}>{this.state. contactno} </Text>
                   <Text style={{ fontSize: 16, color: '#262626' }}>{this.state.address} , {this.state.city} - {this.state.pincode} </Text>
                  </Body>
                </ListItem>
              </List>
              <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', padding: 10 }}>Order Details</Text>
              <List style={{ marginTop: 2 }} dataArray={this.state.cart}
                renderRow={(cartdata) =>
                  <ListItem>
                    <Left>
                      <Text style={{ marginRight: 5 }}>{cartdata.p_name}</Text>
                    </Left>
                    <Left style={{ marginLeft: 90 }}>
                      <Text note>Rs {cartdata.qty * cartdata.p_price}</Text>
                    </Left>
                    <Left style={{ flexDirection: 'row', marginLeft: 40 }}>
                      <Text style={{ paddingRight: 5, paddingLeft: 5 }} note>{cartdata.qty} Qty</Text>
                    </Left>
                  </ListItem>
                }>
              </List>
              <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', padding: 10 }}>Payment  Details</Text>
              <List style={{ marginTop: 2 }}>
                <ListItem>
                  <Left>
                    <Text style={{ marginRight: 5 }}>Sub Total</Text>
                  </Left>
                  <Left style={{ marginLeft: 90 }}>
                    <Text note>Rs {this.state.total}</Text>
                  </Left>
                </ListItem>
                <ListItem>
                  <Left style={{ flexDirection: 'row', marginLeft: 40 }}>
                    <Text style={{ paddingRight: 5, paddingLeft: 5 }} note></Text>
                  </Left>
                </ListItem>
                <ListItem>
                  <Left>
                    <Text style={{ marginRight: 5 }}>Delivery Charge</Text>
                  </Left>
                  <Left style={{ marginLeft: 90 }}>
                    <Text note>Rs {this.state.deliverycharge}</Text>
                  </Left>
                  <Left style={{ flexDirection: 'row', marginLeft: 40 }}>
                    <Text style={{ paddingRight: 5, paddingLeft: 5 }} note></Text>
                  </Left>
                </ListItem>
                <ListItem>
                  <Left>
                    <Text style={{ marginRight: 5, fontWeight: 'bold' }}>Payable Amount</Text>
                  </Left>
                  <Left style={{ marginLeft: 90, }}>
                    <Text note style={{ fontWeight: 'bold' }} >Rs {this.state.total }</Text>
                  </Left>
                  <Left style={{ flexDirection: 'row', marginLeft: 40 }}>
                    <Text style={{ paddingRight: 5, paddingLeft: 5 }} note></Text>
                  </Left>
                </ListItem>
              </List>
              <View style={{ height: 40, marginTop: 25, marginBottom: 50, justifyContent: 'center', alignItems: 'center', }}>
                {this.state.spinner ? <View><Spinner color='#ffb400' /></View> : <View />}
                <TouchableOpacity onPress={() => this.order()} style={{
                  width: screenwidth - 40, height: 50,backgroundColor: "#094872",
                  alignItems: "center", justifyContent: 'center',borderRadius: 5, elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
                }}>
                  <Text style={{ color: "white", fontSize: 20 }}>CONFIRM</Text>
                </TouchableOpacity>
              </View>
              {this.state.spinner ? <View><Spinner color='#ffb400' /></View> : <View />}

           </Content>
           </Container>
     
    );
  }
}


