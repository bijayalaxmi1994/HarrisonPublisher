/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,Dimensions,TouchableOpacity,Image,StatusBar,AsyncStorage,BackHandler
} from 'react-native';
import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail,
     List, ListItem, Tab, Tabs, ScrollableTab,
    Separator,Picker,
    Item, Input, Spinner,Card, CardItem
} from 'native-base';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
export default class order extends Component {
   
        state = {
          ship_email:'',
          order_data:[],
          cart:[],
          open: false,
          spinner:true
              
        }
      
      componentWillMount(){
        
      AsyncStorage.getItem('ship_details')
      .then((value) => {
          if (value != null || value != undefined) {
              const values = JSON.parse(value)
              console.log('user available' + values.ship_email)
              this.setState({ ship_email:values.ship_email  })
             console.log(this.state.ship_email)
 
             var formData = new FormData();
             formData.append('user_email', this.state.ship_email);
             console.log(formData)
             fetch(urldetails.base_url + urldetails.url["ordersum"], {
                 method: 'POST',
                 body: formData
               })
               .then((response)=>response.json())
               .then((jsondata)=>{
                //  console.log(jsondata)
                 if(jsondata.status=="success"){
                //    console.log(jsondata)
                 
                   this.setState({ order_data: jsondata.order,spinner:false
                  });
                   console.log(this.state.order_data)
                 }
                })
     
          }
      })
    }
    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
      this.props.navigation.navigate("Home");
      return true;
    };
    modalDidOpen = () => console.log("Modal did open.");
    modalDidClose = () => {
        this.setState({ open: false });
        console.log("Modal did close.");
    };
    openModal = () => this.setState({ open: true });
    closeModal = () => this.setState({ open: false });
   
  render() {
    return (
     <Container>
         
         <Header style={{ backgroundColor: '#094872' }} hasTabs>
                    <Left>
                        <TouchableOpacity transparent onPress={() => this.props.navigation.openDrawer()}>
                          <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title>Order</Title></Body>
                </Header>
                <StatusBar backgroundColor="#000" barStyle="light-content" />
                 <Content>   
                 {this.state.spinner ? <View style={{marginTop:screenHeight/3}}><Spinner color='#ffb400' /><Text style={{color:'#000',textAlign:'center'}}>Loading...</Text></View> : <View />}      
        <List dataArray={this.state.order_data}
            renderRow={(myitem) =>
                <ListItem style={{borderBottomColor:'#000'}} 
                onPress={()=>this.props.navigation.navigate('Orderdetails',{orderdetails:myitem})}>
                <Body>
                <Text style={{fontSize:16}}>ORDER ID :  {myitem.oid}</Text>
                <Text style={{fontSize:16}}>STATUS : {myitem.o_status}</Text>
                <Text style={{fontSize:16}}>PLACED ON : {myitem.on_date}</Text>
                <Text style={{fontSize:16}}>TOTAL AMOUNT : {myitem.total_price}</Text>
                </Body>
            </ListItem>
            }>
        </List>


        </Content> 
     

                     


   
        
       

            
        
         </Container>
    );
  }
}


